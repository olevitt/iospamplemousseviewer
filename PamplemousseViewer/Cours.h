//
//  Cours.h
//  PamplemousseViewer
//
//  Created by olivier levitt on 10/4/12.
//  Copyright (c) 2012 olivier levitt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cours : NSObject
{
    NSDate *debut;
    NSDate *fin;
    NSString *uid;
    NSString *salle;
    NSString *nom;
}
@property (nonatomic, retain) NSDate *debut;
@property (nonatomic, retain) NSDate *fin;
@property (nonatomic, retain) NSString *uid;
@property (nonatomic, retain) NSString *salle;
@property (nonatomic, retain) NSString *nom;

-(id) init;
-(Cours*) initWithJSON: (id)JSON;
-(Cours*) initWithDebut: (NSDate*)debut AndFin: (NSDate*)fin AndUid: (NSString*) uid AndSalle: (NSString*)salle AndNom: (NSString*)nom;
@end
