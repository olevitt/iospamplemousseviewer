//
//  WindowViewController.m
//  PamplemousseViewer
//
//  Created by olivier levitt on 10/2/12.
//  Copyright (c) 2012 olivier levitt. All rights reserved.
//

#import "WindowViewController.h"
#import "AFNetworking.h"

@implementation WindowViewController

@synthesize cours;
@synthesize tableView;
@synthesize dateFormat;
@synthesize jourFormat;

- (void)viewDidLoad
{ 
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(chargerWebService)];
    self.navigationItem.rightBarButtonItem = refreshButton;
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(demanderIdentifiants)];
    self.navigationItem.leftBarButtonItem = settingsButton;
    self.navigationItem.title = @"PamplemousseViewer";
    if ([tableView respondsToSelector:@selector(registerClass:forCellReuseIdentifier:)]) {
        [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    }
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+1"]];
    jourFormat = [[NSDateFormatter alloc] init];
    [jourFormat setDateFormat:@"EEEE dd MMMM"];
    [jourFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+1"]];
    cours = [[NSMutableArray alloc] init];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *identifiant = [userDefaults stringForKey:@"identifiant"];
    NSString *mdp = [userDefaults stringForKey:@"mdp"];
    if (!identifiant || identifiant == @"" || !mdp || mdp == @"") {
        [self demanderIdentifiants];
    }
    else {
        if ([self lireJSON] != nil) {
            [self chargerTableauAvecJSON:[self lireJSON]];
        }
    }
}

-(void) demanderIdentifiants {
    UIAlertView * alert2 = [[UIAlertView alloc] initWithTitle:@"Vos identifiants pamplemousse" message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    alert2.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    [[alert2 textFieldAtIndex:0] setText:[[NSUserDefaults standardUserDefaults] stringForKey:@"identifiant"]];
    [[alert2 textFieldAtIndex:1] setText:[[NSUserDefaults standardUserDefaults] stringForKey:@"mdp"]];
    [alert2 show];
}

-(void) debutChargement {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.navigationItem.rightBarButtonItem setEnabled:FALSE];
}

-(void) finChargement {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
}

-(void) chargerWebService {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *identifiant = [userDefaults stringForKey:@"identifiant"];
    NSString *mdp = [userDefaults stringForKey:@"mdp"];
    NSString *urlString = [NSString stringWithFormat:@"http://chessdiags.com/pamplemousse/?login=%@&mdp=%@",identifiant,mdp];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [self finChargement];
        [self chargerTableauAvecJSON:JSON];
    }
     
                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
[self finChargement];
UIAlertView * alert2 = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Erreur lors du chargement de l'emploi du temps, merci de reessayer plus tard." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
           alert2.alertViewStyle = UIAlertViewStyleDefault;
   [alert2 show];
                                                                                        }];
    [operation start];
    [self debutChargement];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UITextField *username = [alertView textFieldAtIndex:0];
    UITextField *password = [alertView textFieldAtIndex:1];
    [[NSUserDefaults standardUserDefaults] setValue:username.text forKey:@"identifiant"];
    [[NSUserDefaults standardUserDefaults] setValue:password.text forKey:@"mdp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self chargerWebService];
}

- (void) chargerTableauAvecJSON:(id) JSON
{
    NSArray *events = [JSON objectForKey:@"events"];
    NSLog(@"Emploi du temps: %@", JSON);
    if (events) {
        NSInteger i = 0;
        Cours *courPrecedent;
        Cours *cour;
        [cours removeAllObjects];
        for (i=0; i<events.count; i++) {
            courPrecedent = cour;
            cour = [[Cours alloc] initWithJSON:events[i]];
            if (courPrecedent == Nil || ![[self getJour:courPrecedent.debut] isEqual:[self getJour:cour.debut]]) {
                NSMutableArray *array = [[NSMutableArray alloc] init];
                [array addObject:cour];
                [cours addObject:array];
            }
            else {
                NSMutableArray *array = cours[(cours.count-1)];
                [array addObject:cour];
            }
        }
        [self refreshList];
        NSError *error;
        [self stockerJSON:[NSJSONSerialization dataWithJSONObject:JSON options:0 error:&error]];
    }
    else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Vos identifiants semblent invalides, merci de les vérifier." delegate:nil cancelButtonTitle:@"Hide" otherButtonTitles:nil];
        [alert show];
    }
}

- (NSString *) getJour:(NSDate*) date {
    return [jourFormat stringFromDate:date];
}

- (NSString *) saveFilePath
{
	NSArray *path =
	NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [[path objectAtIndex:0] stringByAppendingPathComponent:@"json.json"];
}

- (void) stockerJSON:(NSData *) JSON
{
    [JSON writeToFile:[self saveFilePath] atomically:NO];
}

- (id) lireJSON
{
    NSString *myPath = [self saveFilePath];
	BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:myPath];
	if (fileExists)
	{
		NSData* content = [NSData dataWithContentsOfFile:myPath];
        NSLog(@"%@",content);
        NSError *error;
        return [NSJSONSerialization JSONObjectWithData:content options:0 error:&error];
	}
    return nil;
}

- (void) refreshList
{
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((NSMutableArray *) cours[section]).count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self getJour:((Cours *) cours[section][0]).debut];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return cours.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView2 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView2 dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == NULL) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    Cours *cour = ((Cours *)((NSMutableArray *) cours[indexPath.section])[indexPath.row]);
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    cell.textLabel.text = [NSString stringWithFormat:@"%@",cour.nom];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@ %@",[dateFormat stringFromDate:cour.debut],[dateFormat stringFromDate:cour.fin],cour.salle];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

@end
