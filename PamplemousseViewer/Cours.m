//
//  Cours.m
//  PamplemousseViewer
//
//  Created by olivier levitt on 10/4/12.
//  Copyright (c) 2012 olivier levitt. All rights reserved.
//

#import "Cours.h"

@implementation Cours
@synthesize nom;
@synthesize salle;
@synthesize uid;
@synthesize debut;
@synthesize fin;

-(id) init {
    self = [Cours alloc];
    return self;
}

-(Cours*) initWithJSON:(id)JSON{
    self = [Cours alloc];
    if (self) {
        self.debut = [NSDate dateWithTimeIntervalSince1970:([JSON[@"debut"] doubleValue])/1000];
        self.fin = [NSDate dateWithTimeIntervalSince1970:([JSON[@"fin"] doubleValue])/1000];
        self.uid = JSON[@"uid"];
        self.nom = JSON[@"nom"];
        self.salle = JSON[@"salle"];
    }
    return self;
}

-(Cours*) initWithDebut:(NSDate *)debut AndFin:(NSDate *)fin AndUid:(NSString *)uid AndSalle:(NSString *)salle AndNom:(NSString *)nom {
    self = [Cours alloc];
    if (self) {
        self.debut = debut;
        self.fin = fin;
        self.uid = uid;
        self.salle = salle;
        self.nom = nom;
    }
    return self;
}

@end
