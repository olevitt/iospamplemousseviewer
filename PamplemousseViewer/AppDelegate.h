//
//  AppDelegate.h
//  PamplemousseViewer
//
//  Created by olivier levitt on 9/26/12.
//  Copyright (c) 2012 olivier levitt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WindowViewController.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) WindowViewController *viewController;

@end
