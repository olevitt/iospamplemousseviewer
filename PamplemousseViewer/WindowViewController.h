//
//  WindowViewController.h
//  PamplemousseViewer
//
//  Created by olivier levitt on 10/2/12.
//  Copyright (c) 2012 olivier levitt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cours.h"

@interface WindowViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
{
    NSMutableArray *cours;
    NSDateFormatter *dateFormat;
    NSDateFormatter *jourFormat;
    IBOutlet UITableView *tableView;
}
@property (nonatomic, retain) NSDateFormatter *dateFormat;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *cours;
@property (nonatomic, retain) NSDateFormatter *jourFormat;

@end
